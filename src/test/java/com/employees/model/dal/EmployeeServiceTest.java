package com.employees.model.dal;
import static org.junit.Assert.*;
import com.employees.model.dal.EmployeeCrud;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.BeforeClass;
import org.hibernate.Session;
import org.junit.AfterClass;
import org.junit.runners.MethodSorters;

import java.time.LocalDate;
import java.util.List;
import com.employees.model.entities.*;
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class EmployeeServiceTest {
	private static EmployeeCrud service;
	
	@BeforeClass
	public static void setUp() {
		service=new EmployeeService();
	}
	
	@AfterClass
	public static void tearDown() {
		//not closing connection during test - for build purposes Exception: EntityManagerFactory is closed
//		service.close();
		service=null;
	}
	
	@Test
	public void testGetEmployees() {
		List<Employee> employs=service.getEmployees();
		assertFalse(employs.isEmpty());
		int amount=0;
		try(Session session=SessionUtil.getSession()){
			amount=((Long)session.createQuery("Select count(*) from Employee e",Long.class).uniqueResult()).intValue();
		}
		assertEquals(amount,employs.size());
	}

	@Test
	public void testGetEmployeesWithParameters() {
		//(String jobId, Long departmentId, Long locationId, String countryId, Integer offset, Integer maxResults)
		List<Employee> mainEmployeesList = service.getEmployeesWithDetails();
		//get employees with job AD_ASST =>1
		long amount = mainEmployeesList.parallelStream().filter(p->p.getJob().getId().equals("AD_ASST")).count();
		List<Employee> empls=service.findEmployees("AD_ASST", null, null, null, null, null);
		assertNotNull(empls);
		assertFalse(empls.isEmpty());
		assertEquals(amount,empls.size());

		// get employees with job SH_CLERK =>20
		amount = mainEmployeesList.parallelStream().filter(p->p.getJob().getId().equals("SH_CLERK")).count();
		empls=service.findEmployees("SH_CLERK", null, null, null, null, null);
		assertFalse(empls.isEmpty());
		assertEquals(amount,empls.size());

		// get employees with job SH_CLERK, retrieve second(20/4) 5 employees, starting with 185,Alexis,Bull,ABULL
		empls=service.findEmployees("SH_CLERK", null, null, null, 5, 5);
		assertFalse(empls.isEmpty());
		assertEquals(5,empls.size());
		Employee emp=empls.get(0);
		assertEquals(new Long(185),emp.getId());
		assertEquals("Bull, Alexis",emp.toString().trim());
		assertEquals("ABULL",emp.getEmail());
		
		//get employees with job SA_REP =>30
		amount = mainEmployeesList.parallelStream().filter(p->p.getJob().getId().equals("SA_REP")).count();
		empls=service.findEmployees("SA_REP", null, null, null,null,null);
		assertFalse(empls.isEmpty());
		assertEquals(amount,empls.size());

		//out of SA_REP get employees with assigned department 80 =>29
		amount = mainEmployeesList.parallelStream().filter(p->{
			return p.getJob().getId().equals("SA_REP")
					&& p.getDepartment()!= null &&
			p.getDepartment().getId().equals(80l);
		}).count();
		empls=service.findEmployees("SA_REP", 80l, null, null,null,null);
		assertFalse(empls.isEmpty());
		assertEquals(amount,empls.size());

		//out of SA_REP get employees with assigned department 80 only the last 10
		amount = mainEmployeesList.parallelStream().filter(p->{
			return p.getJob().getId().equals("SA_REP")
					&& p.getDepartment()!=null && p.getDepartment().getId().equals(80l);
		}).count()-20;
		empls=service.findEmployees("SA_REP", 80l, null, null,20,10);
		assertFalse(empls.isEmpty());
		assertEquals(amount,empls.size());
		
		//get Employees located at department 50 =>45
		amount = mainEmployeesList.parallelStream().filter(p->{
			return p.getDepartment()!=null && p.getDepartment().getId().equals(50l);
		}).count();
		empls=service.findEmployees(null, 50l, null, null,null,null);
		assertFalse(empls.isEmpty());
		assertEquals(amount,empls.size());
		
		//get Employees located at department 50 with jobId ST_MAN =>5
		amount = mainEmployeesList.parallelStream().filter(p->{
			return p.getJob().getId().equals("ST_MAN")
					&& p.getDepartment()!=null && p.getDepartment().getId().equals(50l);
		}).count();

		empls=service.findEmployees("ST_MAN", 50l, null, null,null,null);
		assertFalse(empls.isEmpty());
		assertEquals(amount,empls.size());
		
		//Long locationId, String countryId
		
		//get employees where locationId=1700 =>18
		amount = mainEmployeesList.parallelStream().filter(p->{
			return p.getDepartment()!= null &&p.getDepartment().getLocation().getId().equals(1700l);}
		).count();

		empls=service.findEmployees(null, null, 1700l, null,null,null);
		assertFalse(empls.isEmpty());
		assertEquals(amount,empls.size());

		//get employees where locationId=1700 and jobId= PU_CLERK =>5
		amount = mainEmployeesList.parallelStream().filter(p->{
			return p.getJob().getId().equals("PU_CLERK")
					&& p.getDepartment()!=null && p.getDepartment().getLocation().getId().equals(1700l);
		}).count();

		empls=service.findEmployees("PU_CLERK", null, 1700l, null,null,null);
		assertFalse(empls.isEmpty());
		assertEquals(amount,empls.size());

		//get employees where country is "UK" => 35
		amount = mainEmployeesList.parallelStream().filter(p->{
			return p.getDepartment()!=null && p.getDepartment().getLocation().getCountry().getId().equals("UK");
		}).count();

		empls=service.findEmployees(null, null, null, "UK",null,null);
		assertFalse(empls.isEmpty());
		assertEquals(amount,empls.size());

		//get employees where country is "UK" and jobId= HR_REP =>1
		amount = mainEmployeesList.parallelStream().filter(p->{
			return p.getJob().getId().equals("HR_REP") &&
					p.getDepartment()!=null &&
					p.getDepartment().getLocation().getCountry().getId().equals("UK");
		}).count();

		empls=service.findEmployees("HR_REP", null, null, "UK",null,null);
		assertFalse(empls.isEmpty());
		assertEquals(amount,empls.size());

		//get employees where country is "UK" and department 40 =>1
		amount = mainEmployeesList.parallelStream().filter(p->{
			return p.getDepartment()!=null && p.getDepartment().getId().equals(40l) &&
					p.getDepartment().getLocation().getCountry().getId().equals("UK");
		}).count();

		empls=service.findEmployees(null, 40l, null, "UK",null,null);
		assertFalse(empls.isEmpty());
		assertEquals(amount,empls.size());
		Employee empl=empls.get(0);
		
		assertNotNull(empl.getJob());
		assertNotNull(empl.getDepartment());
		assertNotNull(empl.getDepartment().getLocation());
		assertNotNull(empl.getDepartment().getLocation().getCountry());
	}

	@Test
	public void testOperationsOnEmployee() {
		Employee emp=service.findEmployees("AC_MGR",110l,null,null,null,1).get(0);
		emp.setId(null);
		emp.setFirstName("Antonio");
		emp.setLastName("Banderas");
		emp.setEmail("ABANDERAS");
		emp.setCommission(null);
		emp.setHireDate(LocalDate.of(1982, 3, 13));
		emp.setPhoneNumber("123.123.1234");
		
		//save him
		Employee newEmpl=service.addEmployee(emp);
		assertNotNull(newEmpl);
		assertEquals(newEmpl,emp);
		
		//modify
		emp.setSalary(4500d);
		emp.setEmail("BYLANTONIO");
		
		newEmpl=service.modifyEmployee(emp);
		assertNotNull(newEmpl);
		assertEquals(newEmpl,emp);
		
		//remove
		Long id= emp.getId();
		service.removeEmployee(emp);
		emp=service.findEmployee(id);
		assertNull(emp);
			
	}
	
	@Test
	public void testFindManager() {
		// 124 Kevin,Mourgos,KMOURGOS
		Employee employee = service.findEmployee(124l);
		assertNotNull(employee);
		assertEquals("Mourgos, Kevin",employee.toString().trim());

		Employee manager=service.findManager(124l);
		assertNotNull(manager);
		assertEquals(employee.getManager().toString().trim(),manager.toString().trim());
	}

	@Test
	public void testFindManagers() {
		List<Employee> manags=service.findManagers();
		assertNotNull(manags);
		assertFalse(manags.isEmpty());
		List<Employee> employees = service.getEmployeesWithDetails();
		Employee head = employees.parallelStream().filter(f->f.getManager()==null).findAny().orElse(null);
		long managers = countManagers(employees,head);
		assertEquals(managers,(long)manags.size());
	}

	private static int countManagers(List<Employee> employees, Employee head){
		int amount =0;
		System.out.println(head);
		for (Employee employee : employees) {
			if(employee.getManager() != null && employee.getManager().toString().equals(head.toString()))
				amount++;
		}
		return amount;
	}

	@Test
	public void testGetDepartments() {
		List<Department>depts=service.getDepartments();
		assertFalse(depts.isEmpty());
		assertEquals(27,depts.size());
	}

	@Test
	public void testGetJobs() {
		List<Job>jobs=service.getJobs();
		assertFalse(jobs.isEmpty());
		assertEquals(19,jobs.size());
	}

	@Test
	public void testGetLocations() {
		List<Location>locs=service.getLocations();
		assertFalse(locs.isEmpty());
		assertEquals(23,locs.size());
	}

	@Test
	public void testGetCountries() {
		List<Country>cons=service.getCountries();
		assertFalse(cons.isEmpty());
		assertEquals(25,cons.size());
	}

	@Test
	public void testCountAll() {
		long result=service.employeeCount();
		assertNotEquals(-1,result);
		int amount = service.getEmployees().size();
		assertEquals(amount,result);
	}
}
