package com.employees.model.dal;
import org.junit.Test;
import static org.junit.Assert.*;
import org.hibernate.Session;
import com.employees.model.dal.SessionUtil;
import org.junit.AfterClass;

public class SessionUtilTest {
	@Test
	public void getSessionTest() {
		try(Session session = SessionUtil.getSession()){
			assertNotNull(session);
			session.beginTransaction();
			session.getTransaction().commit();
		}
	}
	
	@Test
	public void getCurrentSessionTest() {
		try(Session session = SessionUtil.getSession()){
			assertNotNull(session);
			Session session2=SessionUtil.getCurrentSession();
			assertNotNull(session2);
			session2.beginTransaction();
			session2.getTransaction().commit();
		}
	}
	
	@AfterClass
	public static void shutdown() {
		//not closing connection during test - for build purposes Exception: EntityManagerFactory is closed
//		SessionUtil.shutdown();
	}
}
