package com.employees.model.entities;
import org.hibernate.Session;
import com.employees.model.dal.SessionUtil;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;

import static org.junit.Assert.*;
import com.employees.model.entities.Job;

import java.lang.reflect.Method;
import java.util.List;
import java.util.Set;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.hibernate.query.Query;

import javax.persistence.RollbackException;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validation;
import javax.validation.ValidatorFactory;
import javax.validation.Validator;
import javax.validation.executable.ExecutableValidator;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class JobTest {
	private static ValidatorFactory vf;
	private Validator validator;
	private static Logger log=LoggerFactory.getLogger(JobTest.class);
	
	@BeforeClass
	public static void setUp() {
		vf=Validation.buildDefaultValidatorFactory();
	}
	
	@AfterClass
	public static void tearDown() {
		vf.close();
	}
	
	@Test
	public void getAll() {
		try(Session session =SessionUtil.getSession()){
			List<Job> jobs=session.getNamedQuery(Job.findAll).getResultList();
			assertNotNull(jobs);
			assertTrue(!jobs.isEmpty());
			assertEquals(jobs.size(),((Long)session.createQuery("Select count(*) from Job j").uniqueResult()).intValue());
		}
	}
	
	@Test
	public void testGetJobById() {
		Job job=null;
		try(Session session=SessionUtil.getSession()){
			Job j=(Job)session.createNamedQuery(Job.findById).setParameter("id","IT_PROG").uniqueResult();
			assertNotNull(j);
			assertEquals(j.getTitle(),"Programmer");
			session.evict(j);
			job=j;
		}
		Job job2=null;
		try(Session session= SessionUtil.getSession()){
			job2=session.load(Job.class, job.getId());
			assertNotNull(job2);
			assertEquals(job2.getTitle(), job.getTitle());
			assertEquals(job2,job);
		}
	}
	
	@Test
	public void testFieldConstraintsValidation() {
		Validator validator=vf.getValidator();
		Job job=new Job("EVK_EVALIk","",4500,3000);
		Set<ConstraintViolation<Job>> violations=validator.validate(job);
		assertFalse(violations.isEmpty());
		assertEquals(4,violations.size());
		
		job.setId("");
		job.setTitle("Evaluator");
		job.setMinSalary(null);
		job.setMaxSalary(null);
		violations=validator.validate(job);
		assertFalse(violations.isEmpty());
		assertEquals(5,violations.size());
		
		job.setId("EV_HRT");
		job.setMinSalary(2500);
		job.setMaxSalary(40000000);
		violations=validator.validate(job);
		assertFalse(violations.isEmpty());
		assertEquals(1,violations.size());
		
		job.setMaxSalary(400000);
		violations=validator.validate(job);
		assertTrue(violations.isEmpty());
		assertEquals(0,violations.size());
	}
	
	@Test
	public void testSetIdParameterValidation() {
		ExecutableValidator ev=vf.getValidator().forExecutables();
		Job job=new Job();
		String id=null;
		try {
			Method method=job.getClass().getDeclaredMethod("setId", String.class);
			Set<ConstraintViolation<Job>>violations=ev.validateParameters(job, method, new Object[] {id});	
			assertFalse(violations.isEmpty());
			assertEquals(1,violations.size());
			assertEquals("Job id was empty",violations.iterator().next().getMessage());
			
			id="EKV_DKF";
			violations=ev.validateParameters(job, method, new Object[] {id});	
			assertFalse(violations.isEmpty());
			assertEquals(1,violations.size());
			assertEquals("Provided Job Id was malformed",violations.iterator().next().getMessage());
			
			id="EK_KDDeDDD";
			violations=ev.validateParameters(job, method, new Object[] {id});	
			assertFalse(violations.isEmpty());
			assertEquals(1,violations.size());
			assertEquals("Provided Job Id was malformed",violations.iterator().next().getMessage());
			
			id="EK_DKLERIDI";
			violations=ev.validateParameters(job, method, new Object[] {id});	
			assertFalse(violations.isEmpty());
			assertEquals(2,violations.size());
			
			id="EK_SKDRLID";
			violations=ev.validateParameters(job, method, new Object[] {id});	
			assertTrue(violations.isEmpty());
			assertEquals(0,violations.size());
		}catch(NoSuchMethodException ex) {
			log.warn(ex.getMessage());
			fail("NoSuchMethodException");
		}catch(Exception ex) {
			log.warn(ex.getMessage());
			fail("Exception");
		}	
	}
	
	@Test
	public void testEnitytSaveFailed() {
		Job job=new Job("EVK_EVALIk","",4500,3000);
		try(Session session=SessionUtil.getSession()){
			session.beginTransaction();
			session.save(job);
			session.getTransaction().commit();
			fail("should not reach here");
		}catch(RollbackException ex) {
			Set<ConstraintViolation<?>> violations=((ConstraintViolationException)ex.getCause()).getConstraintViolations();
			log.info("Failure while persisting an entity: ");
			for(ConstraintViolation v:violations)
				log.info(">>>  "+v.getMessage());
		}catch(ConstraintViolationException ex) {
			log.info("Failure while persisting an entity: ");
			for(ConstraintViolation v:ex.getConstraintViolations())
				log.info(">>>  "+v.getMessage());
		}
		
		job.setTitle("Evaluator");
		job.setId("EV_EVENT");
		try(Session session=SessionUtil.getSession()){
			session.beginTransaction();
			session.save(job);
			session.getTransaction().commit();
			fail("should not reach here");
		}catch(RollbackException ex) {
			Set<ConstraintViolation<?>> violations=((ConstraintViolationException)ex.getCause()).getConstraintViolations();
			log.info("Failure while persisting an entity: ");
			for(ConstraintViolation v:violations)
				log.info(">>>  "+v.getMessage());
		}catch(ConstraintViolationException ex) {
			log.info("Failure while persisting an entity: ");
			for(ConstraintViolation v:ex.getConstraintViolations())
				log.info(">>>  "+v.getMessage());
		}
	}
	

}
