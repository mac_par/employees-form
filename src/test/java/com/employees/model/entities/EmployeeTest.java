package com.employees.model.entities;
import org.hibernate.LazyInitializationException;
import org.hibernate.Session;
import com.employees.model.dal.SessionUtil;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.util.List;
import java.util.Set;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import javax.persistence.RollbackException;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class EmployeeTest {
	private static ValidatorFactory vf;
	private Validator validator;
	private static final Logger log=LoggerFactory.getLogger(EmployeeTest.class);
	
	@BeforeClass
	public static void setUp() {
		vf=Validation.buildDefaultValidatorFactory();
	}
	
	@AfterClass
	public static void tearDown() {
		vf.close();
	}
	
	@Test(expected=LazyInitializationException.class)
	public void findAllTest() {
		Employee emp=null;
		try(Session session=SessionUtil.getSession()){
			List<Employee> empls=session.createNamedQuery(Employee.findAll,Employee.class).list();
			assertNotNull(empls);
			assertFalse(empls.isEmpty());
			assertEquals(((Long)session.createQuery("select count(*) from Employee e").uniqueResult()).intValue(),empls.size());
			emp=empls.get(0);
		}
		log.info(emp.getJob().toString());
		fail("should throw an exception");
	}
	
	@Test
	public void findWithDetailsTest() {
		try(Session session=SessionUtil.getSession()){
			List<Employee> empls=session.createNamedQuery(Employee.findAllWithDetails,Employee.class).setFirstResult(5).setMaxResults(5).list();
			assertNotNull(empls);
			assertFalse(empls.isEmpty());
			assertTrue(empls.size()==5);
			for(Employee e:empls) {
				log.info(e.toString());;
				log.info(">>>> "+e.getFirstName());
				log.info(">>>> "+e.getLastName());
				log.info(">>>> "+e.getEmail());
				log.info(">>>> "+e.getPhoneNumber());
				log.info(">>>> "+ e.getDepartment());
				log.info(">>>> "+e.getJob());
				log.info(">>>> "+e.getManager());
			}
		}
	}
	
	@Test
	public void emplByIdTest() {
		Employee empl=null,emp2=null;
		try(Session session=SessionUtil.getSession()){
			empl=session.createNamedQuery(Employee.findEmployeeById,Employee.class).setParameter("id",201l).uniqueResult();
			assertNotNull(empl);
			assertTrue(empl.getFirstName().equals("Michael"));
			assertTrue(empl.getLastName().equals("Hartstein"));
			emp2=session.byId(Employee.class).load(empl.getId());
			assertNotNull(emp2);
		}
		assertEquals(empl.getManager(),emp2.getManager());
		log.info(empl.getManager().toString()+":"+emp2.getManager().toString());
		assertEquals(empl,emp2);
	}
	
	@Test
	public void violationConstraintsFailedTest() {
		validator=vf.getValidator();
		//get Steven King => 100
		Employee stevenKing = null;
		try(Session session = SessionUtil.getSession()){
			stevenKing = session.byId(Employee.class).load(100l);
		}catch (Exception ex){
			fail(ex.getMessage());
		}
		Employee emp=new Employee("antonio","","","",null,null,null,null,null,null);
		Set<ConstraintViolation<Employee>> violations=validator.validate(emp);
		assertFalse(violations.isEmpty());
		assertEquals(11,violations.size());
		emp.setFirstName("Antonio");
		emp.setLastName("aa");
		emp.setEmail("aa");
		emp.setHireDate(LocalDate.now());
		emp.setSalary(1000.235);
		emp.setManager(stevenKing);
		
		try(Session session=SessionUtil.getSession()){
			emp.setJob(session.byId(Job.class).load("AD_PRES"));
			emp.setDepartment(session.byId(Department.class).load(90l));
			assertNotNull(emp.getJob());
			assertNotNull(emp.getDepartment());
		}catch(Exception ex) {
			fail(ex.getMessage());
		}
		
		violations=validator.validate(emp);
		Employee tmpEmpl = new Employee("Marco","Peublo","MPEUBLO","515.123.4567",
				LocalDate.of(2016,04,21),emp.getJob(),
				1234.0d,.25d,null,emp.getDepartment());

		assertFalse(violations.isEmpty());
		assertEquals(7,violations.size());

		emp.setLastName("banderas");
		emp.setEmail("ABANDERAS");
		emp.setSalary(4500.35);
		emp.setHireDate(LocalDate.of(2019, 2, 21));
		emp.setManager(tmpEmpl);
		emp.setPhoneNumber("235.2453.258");
		
		violations=validator.validate(emp);
		assertFalse(violations.isEmpty());
		assertEquals(3,violations.size());
		
		emp.setLastName("Banderas");
		emp.setEmail("ABANDERAS");
		emp.setSalary(4500d);
		emp.setHireDate(LocalDate.of(2010, 2, 21));
		emp.setCommission(1.2);
		emp.setPhoneNumber("123.1.235");
		
		violations=validator.validate(emp);
		assertFalse(violations.isEmpty());
		for(ConstraintViolation<?> v:violations)
			log.info(v.getMessage());
		assertEquals(2,violations.size());
		
		for(ConstraintViolation<Employee> v:violations)
			log.info(v.getMessage());
		
		try(Session session=SessionUtil.getSession()){
			session.beginTransaction();
			session.save(emp.getManager());
			session.save(emp);
			session.getTransaction().commit();
			fail("shouldnt reach here");
		}catch(RollbackException ex) {
			log.warn(ex.getMessage());
			Set<ConstraintViolation<?>> violation=((ConstraintViolationException)ex.getCause()).getConstraintViolations();
			for(ConstraintViolation<?> v:violation)
				log.info(v.getMessage());
		}catch(ConstraintViolationException ex) {
			Set<ConstraintViolation<?>> violation=ex.getConstraintViolations();
			for(ConstraintViolation<?> v:violation)
				log.info(v.getMessage());
		}

		//remove Marco Peublo from the list
		try(Session session=SessionUtil.getSession()){
			session.beginTransaction();
			log.info(tmpEmpl.getId().toString());
			session.delete(tmpEmpl);
			session.getTransaction().commit();
		}catch(RollbackException ex) {
			log.warn(ex.getMessage());
			fail("shouldnt be here");
		}catch(ConstraintViolationException ex) {
			Set<ConstraintViolation<?>> violation=ex.getConstraintViolations();
			for(ConstraintViolation<?> v:violation)
				log.info(v.getMessage());
			fail("shouldnt be here");
		}
	}
	
	@Test
	public void violationConstraintsPassedTest() {
		//assigning antonio to SA_REP 1 147 80
		Employee emp=new Employee("Antonio","Banderas","ABANDERAS","123.45.4524",LocalDate.of(2010, 3, 22),
				null,5000d,0.15,null,null);
		validator=vf.getValidator();

		Long fId=null;
		try(Session session=SessionUtil.getSession()){
			session.beginTransaction();
			emp.setJob(session.byId(Job.class).load("SA_REP"));
			emp.setDepartment(session.byId(Department.class).load(100l));
			assertEquals(0, validator.validate(emp).size());
			session.save(emp);
			session.getTransaction().commit();
		}catch(RollbackException ex) {
			log.warn(ex.getMessage());
			Set<ConstraintViolation<?>>violation=((ConstraintViolationException)ex.getCause()).getConstraintViolations();
			for(ConstraintViolation<?>v:violation)
				log.warn(v.getMessage());
			fail("shouldnt reach here");
		}
		
		log.info("New Employee was saved under "+emp.getId());
		fId=emp.getId();
		
		try(Session session=SessionUtil.getSession()){
			session.beginTransaction();
			session.delete(emp);
			session.getTransaction().commit();
		}catch(RollbackException ex) {
			log.warn(ex.getMessage());
			fail("shouldnt reach here");
		}
		
		StringBuffer sb=new StringBuffer(30);
		try(Session session=SessionUtil.getSession()){
			session.beginTransaction();
			Employee ep=session.createNamedQuery(Employee.findEmployeeById,Employee.class).setParameter("id", fId).uniqueResult();
			if(ep!=null) {
				sb.append("Object still exists\n");
			}
			Department d=session.createNamedQuery(Department.findById,Department.class).setParameter("id", emp.getDepartment().getId()).uniqueResult();
			if(d==null) {
				sb.append("Department shouldnt be removed");
			}
			session.getTransaction().commit();
		}catch(RollbackException ex) {
			log.warn(ex.getMessage());
			fail("shouldnt reach here");
		}
		
		if(sb.length()>0)
			fail(sb.toString());
//		147
		//create a new employee
				emp=new Employee("Marian","Pazdzioch","MPAZDZIOCH","666.666.6666.666666",LocalDate.of(2000, 4, 2),
				null,5000d,0.2,null,null);
		
		try(Session session=SessionUtil.getSession()){
			session.beginTransaction();
			emp.setJob(session.byId(Job.class).load("SH_CLERK"));
			emp.setDepartment(session.byId(Department.class).load(50l));
			emp.setManager(session.byId(Employee.class).load(147l));
			assertEquals(0, validator.validate(emp).size());
			session.save(emp);
			session.getTransaction().commit();
		}catch(RollbackException ex) {
			log.warn(ex.getMessage());
			fail("shouldnt reach here");
		}
		assertEquals(1,emp.getId()-fId);
		assertEquals(new Long(147l),emp.getManager().getId());
		log.info("Emp: "+emp.getId()+", Dept: "+emp.getDepartment().getId()+","
				+ " Locat:"+emp.getDepartment().getLocation().getId());
		
		try(Session session=SessionUtil.getSession()){
			session.beginTransaction();
			session.delete(emp);
			session.getTransaction().commit();
		}catch(RollbackException ex) {
			log.warn(ex.getMessage());
			fail("shouldnt reach here");
		}
		
		try(Session session=SessionUtil.getSession()){
			session.beginTransaction();
			Employee ep=session.createNamedQuery(Employee.findEmployeeById,Employee.class).setParameter("id", emp.getId()).uniqueResult();
			if(ep!=null)
				fail("object still exists");
			Employee manag = session.byId(Employee.class).load(147l);
			session.getTransaction().commit();
			assertNotNull("Manager should be persived even in case of employee's deletion",manag);
		}catch(RollbackException ex) {
			log.warn(ex.getMessage());
			fail("shouldnt reach here");
		}
	}

	@Test
	public void findManagerTest(){
		Employee ep = null;
		try(Session session=SessionUtil.getSession()){
			session.beginTransaction();
			//emp 174 => 149 Eleni Zlotkey
			ep = session.byId(Employee.class).load(174l);
			session.getTransaction().commit();
		}catch(RollbackException ex) {
			log.warn(ex.getMessage());
			fail("shouldnt reach here");
		}

		assertNotNull(ep);
		assertEquals("Eleni",ep.getManager().getFirstName());
		assertEquals("Zlotkey",ep.getManager().getLastName());
		assertEquals("Zlotkey, Eleni",ep.getManager().toString());
		log.info(ep.getManager().toString());
		ep = null;
		//find by namedQuery
		try(Session session = SessionUtil.getSession()){
			ep = session.createNamedQuery(Employee.findEmployeeById,Employee.class).setParameter("id",149l).uniqueResult();
			assertNotNull(ep);
		}catch(Exception ex){
			log.error("find Manager by @NamedQuery",ex);
			fail("It should not be here");
		}

		assertNotNull(ep.getJob());
		assertNotNull(ep.getManager());
		assertNotNull(ep.getDepartment());
		assertNotNull(ep.getDepartment().getLocation());
		assertNotNull(ep.getDepartment().getLocation().getCountry());
	}
}
