package com.employees.model.entities;
import org.hibernate.Session;
import com.employees.model.entities.Country;

import junit.framework.TestCase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.employees.model.dal.SessionUtil;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import java.util.Iterator;
import java.util.List;

import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import javax.validation.Validation;
import javax.validation.executable.ExecutableValidator;
import javax.persistence.RollbackException;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.Set;
import java.lang.reflect.*;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class CountryTest extends TestCase{
	private static final Logger log = LoggerFactory.getLogger(CountryTest.class);
	private static ValidatorFactory validatorFactory;
	
	@BeforeClass
	public void setUp() {
		validatorFactory=Validation.buildDefaultValidatorFactory();
	}
	
	@AfterClass
	public void tearDown() {
		validatorFactory.close();
	}
	
	@Test
	public void testFindAll() {
		try(Session session=SessionUtil.getSession()){
			List<Country> countries=session.createNamedQuery(Country.findAll,Country.class).list();
			assertNotNull(countries);
			assertEquals(countries.size(),((Long)session.createQuery("Select count(*) from Country c").uniqueResult()).intValue());
			for(Country c:countries)
				log.info(c.toString());
		}
	}
	
	@Test
	public void testFindById() {
		try(Session session=SessionUtil.getSession()){
			Country country=session.createNamedQuery(Country.findById,Country.class).setParameter("id","UK").uniqueResult();
			assertNotNull(country);
			assertEquals(country.getName(),"United Kingdom");
		}
	}
	
	@Test
	public void testIdValidation() throws NoSuchMethodException{
		ExecutableValidator validator=validatorFactory.getValidator().forExecutables();
		Country country=new Country();
		Method method=Country.class.getMethod("setId", String.class);
		String id="USA";
		Set<ConstraintViolation<Country>> violations=validator.validateParameters(country, method, new Object[] {id});
		assertFalse(violations.isEmpty());
		assertEquals(1,violations.size());
		assertEquals("Country Idenitifier should consist of 2 chars only",violations.iterator().next().getMessage());
		id="U";
		violations=validator.validateParameters(country, method, new Object[] {id});
		assertFalse(violations.isEmpty());
		assertEquals(1,violations.size());
		assertEquals("Country Idenitifier should consist of 2 chars only",violations.iterator().next().getMessage());
		
		id="US";
		violations=validator.validateParameters(country, method, new Object[] {id});
		assertTrue(violations.isEmpty());
		assertEquals(0,violations.size());
	}	
	
	@Test
	public void testNameValidation() throws NoSuchMethodException{
		ExecutableValidator validator=validatorFactory.getValidator().forExecutables();
		String name="IR";
		Country country=new Country();
		Method method=Country.class.getDeclaredMethod("setName", String.class);
		Set<ConstraintViolation<Country>> violations=validator.validateParameters(country, method, new Object[] {name});
		assertFalse(violations.isEmpty());
		assertEquals(1,violations.size());
		assertEquals("Country name should consist of at least 3 chars and maximal 40 chars",violations.iterator().next().getMessage());
		name="USSR";
		
		violations=validator.validateParameters(country, method, new Object[] {name});
		assertTrue(violations.isEmpty());
		assertEquals(0,violations.size());
		
		name="Zjednoczone Republiki Bananowe Wielkiego Demiurga i jego przenajświętszej świty Cieni";
		violations=validator.validateParameters(country, method, new Object[] {name});
		assertFalse(violations.isEmpty());
		assertEquals(1,violations.size());
		assertEquals("Country name should consist of at least 3 chars and maximal 40 chars",violations.iterator().next().getMessage());
		
	}
	
	@Test
	public void testSaveViolation() {
		Country country= new Country();
		country.setId("CCCP");
		country.setName("A");
		try(Session session=SessionUtil.getSession()){
			session.beginTransaction();
			session.save(country);
			session.getTransaction().commit();
			fail("should fail here");
		}catch(RollbackException ex) {
			Set<ConstraintViolation<?>>violations=((ConstraintViolationException)ex.getCause()).getConstraintViolations();
			assertEquals(3,violations.size());
		}catch(ConstraintViolationException ex) {
			Set<ConstraintViolation<?>> violations=ex.getConstraintViolations();
			assertEquals(3,violations.size());
		}
		country.setId("PL");
		country.setName("Poland");
		
		try (Session session=SessionUtil.getSession()){
			session.beginTransaction();
			session.save(country);
			session.getTransaction().commit();
			fail("should fail here");
		}catch(RollbackException ex) {
			Set<ConstraintViolation<?>>violations=((ConstraintViolationException)ex.getCause()).getConstraintViolations();
			assertEquals(1,violations.size());
		}catch(ConstraintViolationException ex) {
			Set<ConstraintViolation<?>> violations=ex.getConstraintViolations();
			assertEquals(1,violations.size());
		}	
	}
	
	@Test
	public void testViolationPassed() {
		Validator validator=validatorFactory.getValidator();
		Country country=new Country();
		country.setId("PL");
		country.setName("Poland");
		country.setRegionId(4l);
		Set<ConstraintViolation<Country>>violations=validator.validate(country);
		assertEquals(0, violations.size());
		
		
	}
	@Test
	public void testViolationFailed() {
		Validator validator=validatorFactory.getValidator();
		Country country=new Country();
		country.setId("PLN");
		country.setName("Poland");
		country.setRegionId(4l);
		Set<ConstraintViolation<Country>>violations=validator.validate(country);
		assertEquals(1, violations.size());
		
	}
}
