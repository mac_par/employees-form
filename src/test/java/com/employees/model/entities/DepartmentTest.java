package com.employees.model.entities;
import org.hibernate.Session;
import com.employees.model.dal.SessionUtil;
import org.junit.Test;
import org.junit.BeforeClass;
import org.junit.AfterClass;

import java.util.List;

import javax.validation.*;
import javax.persistence.RollbackException;
import java.util.Set;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

//@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class DepartmentTest extends junit.framework.TestCase{
	private ValidatorFactory factory;
	private Logger log=LoggerFactory.getLogger(DepartmentTest.class);
	
	@BeforeClass
	public void setUp() {
		factory=Validation.buildDefaultValidatorFactory();
	}
	
	@AfterClass
	public void tearDown() {
		factory.close();
	}
	
	@Test
	public void testGetAll() {
		try(Session session=SessionUtil.getSession()){
			List<Department> departs=session.createNamedQuery(Department.findAll,Department.class).list();
			assertNotNull(departs);
			assertFalse(departs.isEmpty());
			assertEquals(departs.size(),((Long)session.createQuery("Select count(*) from Department").uniqueResult()).intValue());
		}
	}
	
	@Test
	public void testFindById() {
		try(Session session=SessionUtil.getSession()){
			Department d=(Department)session.createNamedQuery(Department.findById,Department.class).setParameter("id", 220l).uniqueResult();
			assertNotNull(d);
			assertTrue(d.getId()==220l);
			assertEquals(d.getName(),"NOC");
			assertNotNull(d.getLocation());
		}
	}
	
	@Test
	public void testEqualsLocation() {
		Department depart2=null;
		try(Session session=SessionUtil.getSession()){
			depart2=session.createNamedQuery(Department.findById,Department.class).setParameter("id", 80l).uniqueResult();
			assertNotNull(depart2);
			
		}
		Department depart=null;
		try(Session session=SessionUtil.getSession()){
			depart=session.createNamedQuery(Department.findById,Department.class).setParameter("id", 80l).uniqueResult();
			assertNotNull(depart);
			assertEquals(depart.toString().trim(),depart2.toString().trim());
			assertTrue(depart.equals(depart2));
		}
	}
	
	@Test 
	public void testDepartmentValidationFailed(){
		Validator validator=factory.getValidator();
		Department department=new Department(null,null);
		
		Set<ConstraintViolation<Department>> violations=validator.validate(department);
		assertFalse(violations.isEmpty());
		assertEquals(4,violations.size());
		department.setName("I");
		
		violations=validator.validate(department);
		assertFalse(violations.isEmpty());
		assertEquals(3,violations.size());
		
		department.setName("ITsssssssssssssssssssssssssssssssssssssssssssssssss");
		violations=validator.validate(department);
		assertFalse(violations.isEmpty());
		assertEquals(3,violations.size());
		
		department.setName("IT");
		violations=validator.validate(department);
		assertFalse(violations.isEmpty());
		assertEquals(2,violations.size());

		//get manager
		Employee emp = null;
		try(Session session = SessionUtil.getSession()){
			//201
			emp = session.createNamedQuery(Employee.findEmployeeById,Employee.class).setParameter("id",201l).uniqueResult();
			assertNotNull(emp);
		}catch(Exception ex){
			log.error("getting manager",ex);
			fail("It should not be here");
		}

		department.setId(1234567l);
		department.setManager(null); //invalid
		violations=validator.validate(department);
		assertFalse(violations.isEmpty());

		assertEquals(3,violations.size());//3+11
		
		department.setId(1234l);
		department.setManager(emp);
		department.setLocation(new Location());
		violations=validator.validate(department);
		assertTrue(violations.isEmpty());
		assertEquals(0,violations.size());
		department.setName("");
		
		try(Session session=SessionUtil.getSession()){
			session.beginTransaction();
			Location loc=session.createNamedQuery(Location.findById,Location.class).setParameter("id", 3200l).uniqueResult();
			department.setLocation(loc);
			session.save(department);
			session.getTransaction().commit();
			fail("it shouldnt reach here");
		}catch(RollbackException ex) {
			Set<ConstraintViolation<?>>violat=((ConstraintViolationException)ex.getCause()).getConstraintViolations();
			log.warn(this.getName()+": "+violat.toString());
		}catch(ConstraintViolationException ex) {
			log.warn(this.getName()+": "+ex.getConstraintViolations().toString());
		}
		
	}
	
	@Test 
	public void testDepartmentValidationPassed(){
		//create a dept related to 1700 then delete it
		Location locat=null;
		Department dept=new Department();
		dept.setName("Mikrokorupcja");
		Long managerId = 201l;
		org.hibernate.Transaction tx=null;
		try(Session session=SessionUtil.getSession()){
			tx=session.beginTransaction();
			Employee employee = session.createNamedQuery(Employee.findEmployeeById,Employee.class).setParameter("id",managerId).uniqueResult();
			dept.setManager(employee);
			locat=session.createNamedQuery(Location.findById,Location.class).setParameter("id", 1400l).uniqueResult();
			dept.setLocation(locat);
			session.saveOrUpdate(dept);
			log.info("Department was created "+dept.getId());
			log.info(String.format("Department %s is lead by %s",dept.toString(),dept.getManager().toString()));
			tx.commit();
		}catch(RollbackException ex) {
			tx.rollback();
			Set<ConstraintViolation<?>>violat=((ConstraintViolationException)ex.getCause()).getConstraintViolations();
			fail(this.getName()+": "+violat.toString());
		}catch(ConstraintViolationException ex) {
			tx.rollback();
			fail(this.getName()+": "+ex.getConstraintViolations().toString());
		}

		try(Session session=SessionUtil.getSession()){
			tx=session.beginTransaction();
			dept=session.load(Department.class, dept.getId());
			session.delete(dept);
			log.info(String.format("Result: Dept: %d, Loc: %d, Countr: %s", dept.getId(),dept.getLocation().getId(),dept.getLocation().getCountry().getId()));
			tx.commit();
		}catch(Exception ex) {
			log.warn(ex.getMessage());
			log.info(dept.getId()+"");
			tx.rollback();
			fail("it should be deleted");
		}
		log.info("Department was deleted: "+dept.toString());
		//was it really deleted
		try(Session session=SessionUtil.getSession()){
			session.beginTransaction();
			Department d=session.byId(Department.class).load(dept.getId());
			if(d!=null)
				fail("It should not exist");
			Employee emp = session.byId(Employee.class).load(managerId);
			assertNotNull("manager shouldnt be deleted",emp);
			session.getTransaction().commit();
		}
		Long id=dept.getId();
		//create a dept with a location, then delete it
		dept=new Department();
		dept.setName("Burak");
		locat=new Location();
		dept.setLocation(locat);
		locat.setCity("MAri");
		locat.setPostalCode("66666");
		locat.setStateProvince("Mejbach");
		locat.setStreet("Pana Stanisława z Warszawy");
		
		Country country=new Country();
		country.setId("BL");
		country.setName("Bolanda");
		country.setRegionId(99l);
		locat.setCountry(country);
		
		try(Session session=SessionUtil.getSession()){
			tx=session.beginTransaction();
			dept.setManager(session.byId(Employee.class).load(managerId));
			session.saveOrUpdate(dept.getLocation());
			session.saveOrUpdate(dept);
			tx.commit();
			log.info("Department was established "+dept.getId());
		}catch(RollbackException ex) {
			tx.rollback();
			Set<ConstraintViolation<?>>violat=((ConstraintViolationException)ex.getCause()).getConstraintViolations();
			fail(this.getName()+": "+violat.toString());
		}catch(ConstraintViolationException ex) {
			tx.rollback();
			fail(this.getName()+": "+ex.getConstraintViolations().toString());
		}
		
		
		try(Session session=SessionUtil.getSession()){
			tx=session.beginTransaction();
			session.remove(dept);
			session.remove(dept.getLocation());
			session.remove(dept.getLocation().getCountry());
			tx.commit();
		}catch(Exception ex) {
			fail("it should be deleted");
		}
		
		try(Session session=SessionUtil.getSession()){
			Department de=session.byId(Department.class).load(dept.getId());
			Employee mgr = session.byId(Employee.class).load(managerId);
			if(de!=null)
			fail("not ok;///");
			assertNotNull("manager should still exist",mgr);
			assertEquals(10,dept.getId()-id);
		}catch(Exception ex) {
			log.warn("Exception!!!: "+ex.getMessage());
			fail("it shouldnt be here");
		}
		log.info(String.format("Result: Dept: %d, Loc: %d, Countr: %s", dept.getId(),dept.getLocation().getId(),dept.getLocation().getCountry().getId()));
	}
}
