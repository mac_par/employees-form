package com.employees.model.entities;

import org.hibernate.Session;
import com.employees.model.dal.SessionUtil;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import static org.junit.Assert.*;
import java.util.List;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import java.lang.reflect.Method;
import javax.persistence.RollbackException;
import javax.validation.Validation;
import javax.validation.ValidatorFactory;
import javax.validation.executable.ExecutableValidator;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.Set;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class LocationTest {
	private static ValidatorFactory factory;
	private static final Logger log = LoggerFactory.getLogger(LocationTest.class);

	@BeforeClass
	public static void setUp() {
		factory = Validation.buildDefaultValidatorFactory();
	}

	@AfterClass
	public static void tearDown() {
		factory.close();
	}

	
	@Test
	public void testFindAll() {
		try (Session session = SessionUtil.getSession()) {
			List<Location> locs = session.createNamedQuery(Location.findAll, Location.class).list();
			assertNotNull(locs);
			
			assertEquals((session.createQuery("Select count(*) from Location l",Long.class).uniqueResult()).intValue(),locs.size());
		}
	}

	@Test
	public void testFindById() {
		try (Session session = SessionUtil.getSession()) {
			Location loc = session.createNamedQuery(Location.findById, Location.class).setParameter("id", 3000l)
					.uniqueResult();
			assertNotNull(loc);
			assertTrue(loc.getCity().equals("Bern"));
		}
	}

	@Test
	public void testFindByCountryId() {
		try (Session session = SessionUtil.getSession()) {
			Location loc1 = session.createNamedQuery(Location.findById, Location.class).setParameter("id", 3000l)
					.uniqueResult();
			assertNotNull(loc1);
			List<Location> loc2 = session.createNamedQuery(Location.findByCountryId, Location.class)
					.setParameter("id", "CH").list();
			Location res = loc2.parallelStream().filter(p -> p.getId() == 3000l).findFirst().orElse(null);
			assertNotNull(res);
			assertTrue(loc1.equals(res));
		}
	}

	@Test
	public void testParameterValidation() throws NoSuchMethodException {
		ExecutableValidator validator = factory.getValidator().forExecutables();
		// street
		Location location = new Location();
		String street = null;
		Method method = Location.class.getDeclaredMethod("setStreet", String.class);
		Set<ConstraintViolation<Location>> violations = validator.validateParameters(location, method,
				new Object[] { street });
		assertFalse(violations.isEmpty());
		assertEquals(1, violations.size());
		street = "";
		violations = validator.validateParameters(location, method, new Object[] { street });
		assertFalse(violations.isEmpty());
		assertEquals(2, violations.size());
		street = "dddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddd";
		violations = validator.validateParameters(location, method, new Object[] { street });
		assertFalse(violations.isEmpty());
		assertEquals(1, violations.size());
		street = "100 Muholand Drive Blv.";
		violations = validator.validateParameters(location, method, new Object[] { street });
		assertTrue(violations.isEmpty());
		assertEquals(0, violations.size());

		// postal code
		String postal = "";
		method = Location.class.getDeclaredMethod("setPostalCode", String.class);
		violations = validator.validateParameters(location, method, new Object[] { postal });
		assertTrue(violations.isEmpty());
		assertEquals(0, violations.size());
		postal = "111111111111111111111111111111111111";
		violations = validator.validateParameters(location, method, new Object[] { postal });
		assertFalse(violations.isEmpty());
		assertEquals(1, violations.size());
		postal = "11-100";
		violations = validator.validateParameters(location, method, new Object[] { postal });
		assertTrue(violations.isEmpty());
		assertEquals(0, violations.size());

		// city
		String city = null;
		method = Location.class.getDeclaredMethod("setCity", String.class);
		violations = validator.validateParameters(location, method, new Object[] { city });
		assertFalse(violations.isEmpty());
		assertEquals(1, violations.size());
		city = "";
		violations = validator.validateParameters(location, method, new Object[] { city });
		assertFalse(violations.isEmpty());
		assertEquals(1, violations.size());
		city = "ddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddd";
		violations = validator.validateParameters(location, method, new Object[] { city });
		assertFalse(violations.isEmpty());
		assertEquals(1, violations.size());
		city = "Lidzbark Warmiński";
		violations = validator.validateParameters(location, method, new Object[] { city });
		assertTrue(violations.isEmpty());
		assertEquals(0, violations.size());

		// state
		String state = null;
		method = Location.class.getDeclaredMethod("setStateProvince", String.class);
		violations = validator.validateParameters(location, method, new Object[] { state });
		assertTrue(violations.isEmpty());
		assertEquals(0, violations.size());
		state = "";
		violations = validator.validateParameters(location, method, new Object[] { state });
		assertTrue(violations.isEmpty());
		assertEquals(0, violations.size());
		state = "sssssssssssssssssssssssssss";
		violations = validator.validateParameters(location, method, new Object[] { state });
		assertFalse(violations.isEmpty());
		assertEquals(1, violations.size());
		state = "Warmińsko-mazurskie";
		violations = validator.validateParameters(location, method, new Object[] { state });
		assertTrue(violations.isEmpty());
		assertEquals(0, violations.size());

		// country
		Country country = null;
		method = Location.class.getDeclaredMethod("setCountry", Country.class);
		violations = validator.validateParameters(location, method, new Object[] { country });
		assertFalse(violations.isEmpty());
		assertEquals(1, violations.size());
		country = new Country();
		country.setId("PL");
		country.setName("Poland");
		violations = validator.validateParameters(location, method, new Object[] { country });
		assertTrue(violations.isEmpty());
		assertEquals(0, violations.size());
		country.setRegionId(1l);
		violations = validator.validateParameters(location, method, new Object[] { country });
		assertTrue(violations.isEmpty());
		assertEquals(0, violations.size());

	}
	
	@Test
	public void testValidationPassed() {
		Country country=new Country();
		country.setId("PL");
		country.setName("Poland");
		country.setRegionId(4l);
		String street="100 Muholand Drive Blv.";
		String postal="11-100";
		String city="Lidzbark Warmiński";
		String state="Warmińsko-mazurskie";
		
		Location location=new Location();
		location.setStreet(street);
		location.setPostalCode(postal);
		location.setStateProvince(state);
		location.setCity(city);
		location.setCountry(country);
		
		try(Session session=SessionUtil.getSession()){
			session.beginTransaction();
			session.save(location);
			session.getTransaction().commit();
			log.info("Location created at "+String.format("%s %d",location.getCountry().getName(),location.getId() ));
		}catch(RollbackException ex) {
			Set<ConstraintViolation<?>> violations=((ConstraintViolationException) ex.getCause()).getConstraintViolations();
			log.warn("\n"+violations.toString());
			fail("shouldnt be here");
		}catch(ConstraintViolationException ex) {
			log.warn("\n"+ex.getConstraintViolations().toString());
			fail("shouldnt be here");
		}
		
		try(Session session=SessionUtil.getSession()){
			session.beginTransaction();
			session.remove(location);
			session.getTransaction().commit();
		}catch(Exception ex) {
			log.warn(ex.getMessage());
			fail("a total failure");
		}
		
	}

	@Test
	public void testValidationFailed() {

		String street = "";
		String postal = "11-100";
		String city = "Lidzbark Warmiński";
		String state = "Warmińsko-mazurskie";
		Country country=new Country();
		country.setId("PL");
		country.setName("Poland");
		country.setRegionId(4l);
		Location location = new Location();
		location.setStreet(street);
		location.setPostalCode(postal);
		location.setStateProvince(state);
		location.setCity(city);
		location.setCountry(country);

		try (Session session = SessionUtil.getSession()) {
			session.beginTransaction();
			session.save(location);
			log.info(location.toString());
			session.getTransaction().commit();
			fail("shouldnt reach here");
		} catch (RollbackException ex) {
			Set<ConstraintViolation<?>> violations = ((ConstraintViolationException) ex.getCause())
					.getConstraintViolations();
			log.warn("\n" + violations.toString());
		}catch(ConstraintViolationException ex) {
			log.warn("\n"+ex.getConstraintViolations().toString());
		}
	}
}