package com.employees.controller;
import com.employees.controller.base.MainActionsInterf;
import javafx.scene.layout.BorderPane;

public interface MainWindowController extends MainActionsInterf{
    void setPane(BorderPane pane);
    BorderPane getPane();
}
