package com.employees.controller;
import com.employees.controller.base.AbstractController;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import com.employees.controller.TableTabController;
import javafx.scene.control.TabPane;
import javafx.scene.control.Tab;
import javafx.fxml.FXML;
import java.io.IOException;
import java.net.URL;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

public class MainWindowImpl extends AbstractController<BorderPane> implements MainWindowController{
    private static final Logger log = LoggerFactory.getLogger(MainWindowImpl.class);

    private TableTabController tableTabController;
    @FXML private TabPane tabPane;
    @FXML private Tab tableTab;
    @FXML private Tab detailTab;

    @Override
    protected void initialize() {
        initTabs();
    }

    private void initTabs(){
        //tableTab
        prepareTableTab();
    }

    private void prepareTableTab(){
        try {
            FXMLLoader loader = getFxmlLoader("tableTab.fxml");
            AnchorPane pane = loader.load();
            this.tableTabController = loader.getController();
            this.tableTabController.setPane(pane);
        }catch(IOException ex){
            log.error("prepareTableTab",ex);
        }
        this.tableTab.setContent(this.tableTabController.getPane());
    }

    private void prepareDetailTab(){
        try {
            FXMLLoader loader = getFxmlLoader("detailTab.fxml");
            AnchorPane pane = loader.load();
            this.tableTabController = loader.getController();
            this.tableTabController.setPane(pane);
        }catch(IOException ex){
            log.error("prepareTableTab",ex);
        }
    }

    private FXMLLoader getFxmlLoader(String file){
            URL resource = MainWindowImpl.getResourceFile(file);
            return new FXMLLoader(resource);
    }

    private static URL getResourceFile(String path){
        return MainWindowImpl.class.getResource("/scenes/"+path);
    }

    @Override
    public void close() {
        this.tableTabController.close();
    }
}
