package com.employees.controller.base;
import javafx.scene.layout.Pane;
import javafx.fxml.FXML;

abstract public class AbstractController<T extends Pane> {
    private T pane;

    public AbstractController() {
    }

    @FXML
    abstract protected void initialize();

    abstract public void close();

    public T getPane(){
        return this.pane;
    }

    public void setPane(T pane) {
        this.pane = pane;
    }
}
