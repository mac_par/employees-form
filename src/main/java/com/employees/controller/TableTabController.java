package com.employees.controller;
import com.employees.controller.base.MainActionsInterf;
import javafx.scene.layout.AnchorPane;

public interface TableTabController extends MainActionsInterf{
	void setMaxResultsPerPage(int amount);
	void refresh();
	void setPane(AnchorPane pane);
	AnchorPane getPane();
}
