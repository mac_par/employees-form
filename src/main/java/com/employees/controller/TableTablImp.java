package com.employees.controller;

import com.employees.model.entities.Employee;
import com.employees.controller.base.AbstractController;
import javafx.fxml.FXML;
import javafx.scene.control.TableView;
import javafx.scene.control.TableColumn;
import javafx.collections.ObservableList;
import javafx.collections.FXCollections;
import com.employees.model.dal.EmployeeCrud;
import com.employees.model.dal.EmployeeService;
import javafx.scene.control.cell.PropertyValueFactory;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import com.employees.model.entities.Job;
import javafx.scene.control.Tab;
import javafx.scene.layout.AnchorPane;


public class TableTablImp extends AbstractController<AnchorPane> implements TableTabController {
    private static final Logger logger = LoggerFactory.getLogger(TableTablImp.class);

    //constant values declaration
    private static final int MAX_RESULTS_PER_PAGE = 25;
    private int maxResults = MAX_RESULTS_PER_PAGE;
    private long totalAmount;
    @FXML private TableView<Employee> mainTableView;
    @FXML private TableColumn<Employee, String> surnameColumn;
    @FXML private TableColumn<Employee, String> firstNameColumn;
    @FXML private TableColumn<Employee, Job> titleColumn;
    @FXML private TableColumn<Employee, String> emailColumn;
    @FXML private TableColumn<Employee, String> phoneColumn;
    @FXML private TableColumn<Employee, String> departmentColumn;
    @FXML private TableColumn<Employee, String> managerColumn;
    @FXML private Tab entryTab;
    @FXML private Tab tableTab;
    private ObservableList<Employee> employeesList;
    private final EmployeeCrud employeeDao;

    public TableTablImp() {
        this.employeeDao = new EmployeeService();
    }

    @Override
    protected void initialize() {
        updateTotalAmount();
        initEmployeeList();
        initTableView();
        refreshEmployeeTable();
    }

    private void initTableView(){
        this.surnameColumn.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        this.firstNameColumn.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        this.titleColumn.setCellValueFactory(new PropertyValueFactory<>("job"));
        this.emailColumn.setCellValueFactory(new PropertyValueFactory<>("email"));
        this.phoneColumn.setCellValueFactory(new PropertyValueFactory<>("phoneNumber"));
        this.departmentColumn.setCellValueFactory(new PropertyValueFactory<>("department"));
        this.managerColumn.setCellValueFactory(new PropertyValueFactory<>("manager"));

        this.mainTableView.getSelectionModel().selectedIndexProperty()
                .addListener((obsv,oldVal, newVal)->{
                    //TODO
                    logger.info("pass an object to tab");
                });

    }

    private void initEmployeeList(){
        this.employeesList = FXCollections.observableList(this.employeeDao.findEmployees(null,null,
                null,null,0,this.maxResults));
    }

    private void refreshEmployeeTable(){
        this.mainTableView.setItems(this.employeesList);
        this.mainTableView.refresh();
    }

    @Override
    public void close() {
        this.mainTableView.getItems().clear();
        this.mainTableView = null;
        this.employeeDao.close();
    }

    @Override
    public void setMaxResultsPerPage(int amount) {
        this.maxResults = amount;

    }

    @Override
    public void refresh() {
       refreshEmployeeTable();
    }

    private void updateTotalAmount(){
        this.totalAmount = this.employeeDao.employeeCount();
    }

    @Override
    public void setPane(AnchorPane pane) {
        super.setPane(pane);
    }

    @Override
    public AnchorPane getPane() {
        return super.getPane();
    }
}
