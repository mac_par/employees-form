package com.employees.view;
import com.employees.controller.MainWindowController;
import javafx.application.Application;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.scene.layout.Pane;
import javafx.scene.Scene;
import javafx.fxml.FXMLLoader;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import java.net.URL;

public class MainWindow extends Application{
    private static final Logger log = LoggerFactory.getLogger(MainWindow.class);

    private static final String APP_TITLE = "Employees Summary";
    private MainWindowController mainWindowController;

    @Override
    public void start(Stage primaryStage) throws Exception {
        initScenes();
        Pane pane = this.mainWindowController.getPane();
        Scene scene = new Scene(pane,900,700, Color.LIGHTSLATEGRAY);
        primaryStage.setScene(scene);
        primaryStage.setTitle(APP_TITLE);
        primaryStage.show();
    }

    private void initScenes(){
        try {
            URL resource = MainWindow.getResourceFile("mainWindow.fxml");
            FXMLLoader loader = new FXMLLoader(resource);
            BorderPane borderPane = loader.load();
            this.mainWindowController = loader.getController();
            this.mainWindowController.setPane(borderPane);
        }catch (Exception ex){
            log.error("initScenes",ex);
            System.exit(1);
        }
    }

    private static URL getResourceFile(String path){
        return MainWindow.class.getResource("/scenes/"+path);
    }
    @Override
    public void init() throws Exception {
        super.init();
    }

    @Override
    public void stop() throws Exception {
        this.mainWindowController.close();
        super.stop();
    }

    public static void main(String[] args){
        launch(args);
    }
}
