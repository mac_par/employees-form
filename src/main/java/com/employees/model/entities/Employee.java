package com.employees.model.entities;
import javax.persistence.*;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import com.employees.model.entities.validation.Commission;
import com.employees.model.entities.validation.PhoneNumber;
import java.time.LocalDate;

@Entity
@Table(name="EMPLOYEES",uniqueConstraints= {@UniqueConstraint(columnNames={"FIRST_NAME","LAST_NAME"})})
@NamedQueries({
	@NamedQuery(name="Employees.countAll",query="Select distinct count(e) from Employee e"),
	@NamedQuery(name="Employees.findAll",query="from Employee e"),
	@NamedQuery(name="Employees.findAllWithDetails",query="from Employee e left join fetch e.job j left join fetch e.department d" +
			" left join fetch d.location l"),
	@NamedQuery(name="Employees.findEmployeeById",query="from Employee e left join fetch e.job j left join fetch e.department d"
			+ " left join fetch d.location l where e.id=:id"),
	})
public class Employee implements java.io.Serializable{
	public static final String countAll="Employees.countAll";
	public static final String findAll="Employees.findAll";
	public static final String findAllWithDetails="Employees.findAllWithDetails";
	public static final String findEmployeeById="Employees.findEmployeeById";

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="generator")
	@SequenceGenerator(name="generator",sequenceName="EMPLOYEES_SEQ",allocationSize=1)
	@Digits(integer=6,fraction=0)
	@Column(name="EMPLOYEE_ID",scale=6)
	private Long id;
	
	@Size(min=3,max=20,message="First name should consist of {min}-{max} characters")
	@Pattern(regexp="^([A-Z]{1}\\w{2,19})",message="First name was malformed")
	@Column(name="FIRST_NAME")
	private String firstName;
	@NotBlank
	@Pattern(regexp="^([A-Z]{1}\\w{2,24})",message="Last name was malformed")
	@Size(min=3,max=25,message="Last name should consist of {min}-{max} characters")
	@Column(name="LAST_NAME")
	private String lastName;
	
	@NotBlank(message="Email should be provided")
	@Pattern(regexp="^[A-Z]{3,25}$",message="Email was malformed")
	@Size(min=3,max=25,message="Email should consist of {min}-{max} characters")
	@Column(name="EMAIL",unique=true)
	private String email;
	
	@PhoneNumber
	@Column(name="PHONE_NUMBER")
	private String phoneNumber;
	
	@Past
	@NotNull(message="Hire date should be provided")
	@Column(name="HIRE_DATE")
	private LocalDate hireDate;

	@NotNull(message="Job should exists")
	@ManyToOne(fetch=FetchType.LAZY,cascade= {CascadeType.MERGE,CascadeType.PERSIST,CascadeType.REFRESH,CascadeType.DETACH})
	@JoinColumn(name="JOB_ID")
	private Job job;
	
	@NotNull(message="Salary should be provided")
	@Digits(integer=8,fraction=2)
	@Column(name="SALARY",precision=2,scale=8)
	private Double salary;
	
	@Commission
	@Column(name="COMMISSION_PCT")
	private Double commission;
	
	@ManyToOne(fetch=FetchType.LAZY,cascade={CascadeType.MERGE,CascadeType.PERSIST,CascadeType.REFRESH,CascadeType.DETACH})
	@JoinColumn(name="DEPARTMENT_ID")
	private Department department;

	@ManyToOne(cascade ={CascadeType.DETACH,CascadeType.PERSIST,CascadeType.REFRESH,CascadeType.DETACH})
	@JoinColumn(name = "MANAGER_ID")
	private Employee manager;

	public Employee() {}
	
	public Employee(String firstName,String lastName,
			String email,String phoneNumber,LocalDate hireDate,Job job, Double salary,
			Double commission, Employee manager, Department department) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.phoneNumber=phoneNumber;
		this.hireDate = hireDate;
		this.job = job;
		this.salary = salary;
		this.commission = commission;
		this.manager = manager;
		this.department = department;
	}

	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public LocalDate getHireDate() {
		return hireDate;
	}

	public void setHireDate(LocalDate hireDate) {
		this.hireDate = hireDate;
	}

	public Job getJob() {
		return job;
	}

	public void setJob(Job job) {
		this.job = job;
	}

	public Double getSalary() {
		return salary;
	}

	public void setSalary(Double salary) {
		this.salary = salary;
	}

	public Double getCommission() {
		return commission;
	}

	public void setCommission(Double commission) {
		this.commission = commission;
	}

	public Department getDepartment() {
		return department;
	}

	public void setDepartment(Department department) {
		this.department = department;
	}
	

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public Employee getManager() {
		return manager;
	}

	public void setManager(Employee manager) {
		this.manager = manager;
	}

	@Override
	public int hashCode() {
		int result =this.id!=null?this.id.hashCode():0;
		result+=this.firstName!=null?this.firstName.hashCode():0;
		result+=this.lastName!=null?this.lastName.hashCode():0;
		result+=this.email!=null?this.email.hashCode():0;
		result+=this.phoneNumber!=null?this.phoneNumber.hashCode():0;
		result+=this.hireDate!=null?this.hireDate.hashCode():0;
		result+=this.job!=null?this.job.hashCode():0;
		result+=this.salary!=null?this.salary.hashCode():0;
		result+=this.commission!=null?this.commission.hashCode():0;
		result+=this.manager!=null?this.manager.hashCode():0;
		result+=this.department!=null?this.department.hashCode():0;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if(obj==null)
			return false;
		if(this==obj)
			return true;
		if(!this.getClass().equals(obj.getClass()))
			return false;
		Employee emp=(Employee)obj;
		if(this.id!=null?!this.id.equals(emp.id):emp.id!=null)
			return false;
		if(this.firstName!=null?!this.firstName.equals(emp.firstName):emp.firstName!=null)
			return false;
		if(!this.lastName.equals(emp.lastName))
			return false;
		if(!this.email.equals(emp.email))
			return false;
		if(this.phoneNumber!=null?!this.phoneNumber.equals(emp.phoneNumber):emp.phoneNumber!=null)
			return false;
		if(!this.hireDate.equals(emp.hireDate))
			return false;
		if(this.job != null ? !this.job.equals(emp.job) : emp.job!=null)
			return false;
		if(this.salary!=null?!this.salary.equals(emp.salary):emp.salary!=null)
			return false;
		if(this.commission!=null?!this.commission.equals(emp.commission):emp.commission!=null)
			return false;
		if(this.manager!=null?!this.manager.equals(emp.manager):emp.manager!=null)
			return false;
		if(this.department!=null?!this.department.equals(emp.department):emp.department!=null)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return String.format("%s, %s",this.lastName,this.firstName).trim();
	}
	
	
}
