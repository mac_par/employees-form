package com.employees.model.entities;
import javax.persistence.*;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import com.employees.model.entities.validation.JobId;
import com.employees.model.entities.validation.ValidSalary;

@Entity
@Table(name="JOBS")
@NamedQueries({
	@NamedQuery(name="Job.findAll",query="from Job j"),
	@NamedQuery(name="Job.findById",query="from Job j where j.id=:id")
})
@ValidSalary
public class Job implements java.io.Serializable{
	public static final String findAll="Job.findAll";
	public static final String findById="Job.findById";
	
	@Id
	@JobId
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="JOB_ID")
	private String id;
	@NotBlank(message="Job title was empty")
	@Size(min=2,max=35,message="Job title should consist of at least {min} characters and maximum {max} characters")
	@Column(name="JOB_TITLE",unique=true)
	private String title;
	
	@Column(name="MIN_SALARY")
	@Digits(integer=6,fraction=0)
	private Integer minSalary;
	@Column(name="MAX_SALARY")
	@Digits(integer=6,fraction=0)
	private Integer maxSalary;
	
	public Job() {}
	
	@ValidSalary
	public Job(String id,String title,int minSalary,int maxSalary) {
		this.id=id;
		this.title=title;
		this.minSalary=minSalary;
		this.maxSalary=maxSalary;
	}
	public String getId() {
		return id;
	}
	public void setId(@JobId String id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public Integer getMinSalary() {
		return minSalary;
	}
	public void setMinSalary(Integer minSalary) {
		this.minSalary = minSalary;
	}
	public Integer getMaxSalary() {
		return maxSalary;
	}
	public void setMaxSalary(Integer maxSalary) {
		this.maxSalary = maxSalary;
	}
	@Override
	public int hashCode() {
		int result=this.id.hashCode();
		result+=this.title.hashCode()*3;
		result+=this.minSalary.hashCode();
		result+=this.maxSalary.hashCode();
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if(obj==null)
			return false;
		if(this==obj)
			return true;
		if(!this.getClass().equals(obj.getClass()))
			return false;
		Job jb=(Job) obj;
		if(this.id!=null?!this.id.equals(jb.id):jb.id!=null)
			return false;
		if(this.title!=null?!this.title.equals(jb.title):jb.title!=null)
			return false;
		if(this.minSalary!=null?!this.minSalary.equals(jb.minSalary):jb.minSalary!=null)
			return false;
		if(this.maxSalary!=null?!this.maxSalary.equals(jb.maxSalary):jb.maxSalary!=null)
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return this.title;
	}	
}