package com.employees.model.entities;
import javax.persistence.*;
import org.hibernate.validator.constraints.*;
import javax.validation.constraints.*;
import javax.validation.constraints.NotBlank;

@Entity
@Table(name="COUNTRIES")
@NamedQueries({
	@NamedQuery(name="Country.findAll",query="from Country c"),
	@NamedQuery(name="Country.findById",query="from Country c where c.id=:id")
})
public class Country implements java.io.Serializable{
	public static final String findAll="Country.findAll";
	public static final String findById="Country.findById";
	@Id
	@Size(min=2,max=2,message="Country Idenitifier should consist of {min} chars only")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="COUNTRY_ID")
	private String id;
	
	@NotBlank
	@Length(min=3,max=40,message="Country name should consist of at least {min} chars and maximal {max} chars")
	@Column(name="COUNTRY_NAME",unique=true)
	private String name;
	
	@NotNull
	@Column(name="REGION_ID")
	private Long regionId;
	
	public Country() {}

	public String getId() {
		return id;
	}
	
	public void setId(@Size(min=2,max=2,message="Country Idenitifier should consist of {min} chars only")String id) {
		this.id=id;
	}

	public String getName() {
		return name;
	}

	public void setName(@Length(min=3,max=40,message="Country name should consist of at least {min} chars and maximal {max} chars") String name) {
		this.name = name;
	}

	public Long getRegionId() {
		return regionId;
	}

	public void setRegionId(@NotNull Long regionId) {
		this.regionId = regionId;
	}

	@Override
	public int hashCode() {
		int result=this.id!=null?this.id.hashCode():0;
		result+=this.name!=null?this.name.hashCode():0;
		result+=this.regionId!=null?this.regionId.hashCode():0;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if(obj==null)
			return false;
		if(this==obj)
			return true;
		if(!this.getClass().equals(obj.getClass()))
			return false;
		Country ct=(Country)obj;
		if(this.id!=null?!this.id.equals(ct.id):ct.id!=null)
			return false;
		if(!this.name.equals(ct.name))
			return false;
		if(!this.regionId.equals(ct.regionId))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return this.name;
	}
	
}