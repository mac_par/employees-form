package com.employees.model.entities.validation;
import javax.validation.Constraint;
import javax.validation.Payload;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import java.lang.annotation.Target;
import java.lang.annotation.Retention;
import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;

@Constraint(validatedBy={})
@NotBlank(message="Job id was empty")
@Size(min=2,max=10,message="Job id should consist of at least {min} characters and maximum {max} characters")
@Pattern(regexp="^([A-Z]{2}_[A-Z]{2,7})$",message="Provided Job Id was malformed")
@Retention(RUNTIME)
@Target({METHOD,TYPE,PARAMETER,FIELD})
@Documented
public @interface JobId {
	String message() default "";
	Class<?>[] groups() default {};
	Class<? extends Payload>[] payload() default {};
}