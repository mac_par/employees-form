package com.employees.model.entities.validation;
import java.lang.annotation.Retention;
import static java.lang.annotation.RetentionPolicy.*;
import java.lang.annotation.Documented;
import java.lang.annotation.Target;
import static java.lang.annotation.ElementType.*;
import javax.validation.Constraint;
import javax.validation.Payload;


@Target({TYPE,CONSTRUCTOR})
@Retention(RUNTIME)
@Constraint(validatedBy=SalaryValidator.class)
@Documented
public @interface ValidSalary {
	String message() default "minimal salary should be less than maximal salary";
	Class<?>[] groups() default {};
	Class<? extends Payload>[] payload() default {};
}
