package com.employees.model.entities.validation;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import com.employees.model.entities.Job;

public class SalaryValidator implements ConstraintValidator<ValidSalary, Job>{

	@Override
	public boolean isValid(Job job, ConstraintValidatorContext ctx) {
		if(job.getMinSalary()==null) {
			ctx.disableDefaultConstraintViolation();
			ctx.buildConstraintViolationWithTemplate("Minimal salary was not provided").addConstraintViolation();
		}
		if(job.getMaxSalary()==null) {
			ctx.disableDefaultConstraintViolation();
			ctx.buildConstraintViolationWithTemplate("Maximal salary was not provided").addConstraintViolation();
		}

		Integer min=job.getMinSalary();
		Integer max=job.getMaxSalary();
		return ((min!=null && max!=null) && min<max);
	}

}
