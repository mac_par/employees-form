package com.employees.model.entities.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.employees.model.entities.Employee;

public class CommissionValidator implements ConstraintValidator<Commission, Double> {

	@Override
	public boolean isValid(Double cms, ConstraintValidatorContext arg1) {
		if(cms==null)
			return true;
		return Double.compare(cms%cms.intValue(), Double.NaN)==0;
	}

}
