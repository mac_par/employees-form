package com.employees.model.entities.validation;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.*;
import javax.validation.Constraint;
import javax.validation.ReportAsSingleViolation;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import javax.validation.Payload;

@Constraint(validatedBy={})
@NotBlank(message="Phone number shouldnt be empty")
@Pattern(regexp="^\\d{3}.\\d{2,3}.\\d{4}(.\\d{6})?$",message="Phone number was malformed")
@Size(min=3,max=20,message="Phone number should consist of at least {min} and maximal {max} characters")
@ReportAsSingleViolation
@Retention(RUNTIME)
@Target({FIELD,PARAMETER,TYPE})
public @interface PhoneNumber {
	String message() default "Phone number is incorrect";
	Class<?>[] groups() default {};
	Class<? extends Payload>[] payload() default {};
}
