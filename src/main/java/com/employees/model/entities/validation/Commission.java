package com.employees.model.entities.validation;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.*;
import javax.validation.Constraint;
import javax.validation.constraints.Digits;
import javax.validation.Payload;

@Constraint(validatedBy={CommissionValidator.class})
@Digits(integer=2,fraction=2)
@Retention(RUNTIME)
@Target({FIELD,PARAMETER,TYPE})
public @interface Commission {
	String message() default "Commission: provided value should be less than 1";
	Class<?>[] groups() default {};
	Class<? extends Payload>[] payload() default {};
}
