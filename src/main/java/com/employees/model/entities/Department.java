package com.employees.model.entities;
import javax.persistence.*;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name="DEPARTMENTS",uniqueConstraints= {@UniqueConstraint(columnNames= {"DEPARTMENT_ID","DEPARTMENT_NAME"})})
@NamedQueries({
	@NamedQuery(name="Department.findAll",query="from Department d left join fetch d.manager m " +
			"left join fetch d.location l left join fetch l.country c"),
	@NamedQuery(name="Department.findById",query="from Department d left join fetch d.location l " +
			"left join fetch l.country c where d.id=:id")
})


public class Department implements java.io.Serializable{
	public static final String findAll="Department.findAll";
	public static final String findById="Department.findById";
	@Id
	@Digits(integer=4,fraction=0)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="department_generator")
	@SequenceGenerator(name="department_generator",sequenceName="DEPARTMENTS_SEQ",allocationSize=1)
	@Column(name="DEPARTMENT_ID")
	private Long id;
	
	@NotNull
	@NotBlank
	@Size(min=2,max=30,message="DepartmentName should be between 2-30")
	@Column(name="DEPARTMENT_NAME",unique=true)
	private String name;
	
	@NotNull
	@OneToOne(orphanRemoval = false, cascade = {CascadeType.DETACH,CascadeType.PERSIST,CascadeType.MERGE,CascadeType.REFRESH})
	@JoinColumn(name = "MANAGER_ID")
	private Employee manager;

	@NotNull
	@ManyToOne(cascade= {CascadeType.DETACH,CascadeType.MERGE,CascadeType.PERSIST,CascadeType.REFRESH},fetch=FetchType.LAZY)
	@JoinColumn(name="LOCATION_ID")
	private Location location;
	
	public Department() {}
	public Department(String name, Location location) {
		this.name=name;
		this.location=location;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public Employee getManager() {
		return manager;
	}

	public void setManager(Employee manager) {
		this.manager = manager;
	}

	@Override
	public int hashCode() {
		int result=this.id!=null?this.id.hashCode():0;
		result+=this.name!=null?this.name.hashCode():0;
		result+=this.manager!=null?this.manager.hashCode():0;
		result+=this.location!=null?this.location.hashCode():0;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if(obj==null)
			return false;
		if(this==obj)
			return true;
		if(!this.getClass().equals(obj.getClass()))
			return false;
		Department dep=(Department) obj;
		if(this.id!=null?!this.id.equals(dep.id):dep.id!=null)
			return false;
		if(!this.name.equals(dep.name))
			return false;
		if(this.manager!=null? !this.manager.toString().equals(dep.manager.toString()):dep.manager!=null)
			return false;
		if(!this.location.equals(dep.location))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return this.name;
	}
}