package com.employees.model.entities;
import javax.validation.constraints.*;
import javax.validation.constraints.NotBlank;
import javax.persistence.*;

@Entity
@Table(name="LOCATIONS")
@NamedQueries({
	@NamedQuery(name="Location.findAll",query="from Location l"),
	@NamedQuery(name="Location.findById",query="from Location l left join fetch l.country where l.id=:id"),
	@NamedQuery(name="Location.findByCountryId",query="from Location l left join fetch l.country where l.country.id=:id")
})
public class Location implements java.io.Serializable{
	public static final String findAll="Location.findAll";
	public static final String findById="Location.findById";
	public static final String findByCountryId="Location.findByCountryId";
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="gen")
	@SequenceGenerator(name="gen",sequenceName="LOCATIONS_SEQ",allocationSize=1)
	@Digits(fraction=0,integer=4)
	@Column(scale=4,name="LOCATION_ID")
	private Long id;
	
	@NotBlank
	@NotNull
	@Size(max=40,min=8)
	@Column(name="STREET_ADDRESS")
	private String street;
	
	@Size(max=12)
	@Column(name="POSTAL_CODE")
	private String postalCode;
	
	@NotBlank
	@Size(max=30)
	@NotNull
	@Column(name="CITY",nullable=true)
	private String city;
	
	@Size(max=25)
	@Column(name="STATE_PROVINCE",nullable=true)
	private String stateProvince;
	
	@NotNull
	@ManyToOne(cascade={CascadeType.PERSIST,CascadeType.MERGE,CascadeType.DETACH,CascadeType.REFRESH},optional=false)
	@JoinColumn(name="COUNTRY_ID")
	private Country country;
	
	public Location() {}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(@NotBlank @Size(max=40,min=8) String street) {
		this.street = street;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(@Size(max=12) String postalCode) {
		this.postalCode = postalCode;
	}

	public String getCity() {
		return city;
	}

	public void setCity(@NotBlank @Size(max=30) String city) {
		this.city = city;
	}

	public String getStateProvince() {
		return stateProvince;
	}

	public void setStateProvince(@Size(max=25) String stateProvince) {
		this.stateProvince = stateProvince;
	}

	public Country getCountry() {
		return country;
	}

	public void setCountry(@NotNull Country country) {
		this.country = country;
	}

	@Override
	public int hashCode() {
		int result=this.id!=null?this.id.hashCode():0;
		result+=this.street!=null?this.street.hashCode():0;
		result+=this.postalCode!=null?this.postalCode.hashCode():0;
		result+=this.city!=null?this.city.hashCode():0;
		result+=this.stateProvince!=null?this.stateProvince.hashCode():0;
		result+=this.country!=null?this.country.hashCode():0;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if(obj==null)
			return false;
		if(this==obj)
			return true;
		if(!this.getClass().equals(obj.getClass()))
			return false;
		Location loc=(Location)obj;
		if(this.id!=null?!this.id.equals(loc.id):loc.id!=null)
			return false;
		if(this.street!=null?!this.street.equals(loc.street):loc.street!=null)
			return false;
		if(this.postalCode!=null?!this.postalCode.equals(loc.postalCode):loc.postalCode!=null)
			return false;
		if(this.city!=null?!this.city.equals(loc.city):loc.city!=null)
			return false;
		if(this.stateProvince!=null?!this.stateProvince.equals(loc.stateProvince):loc.stateProvince!=null)
			return false;
		if(this.country!=null?!this.country.equals(loc.country):loc.country!=null)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return this.city;
	}
}