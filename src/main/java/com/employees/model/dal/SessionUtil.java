package com.employees.model.dal;
import org.hibernate.cfg.Configuration;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.boot.MetadataSources;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.concurrent.Semaphore;

//TODO change class visibility to package visibility, however, as for now it requires to
//get rid of most of entity tests and rely on EmployeeService class

public class SessionUtil {
	private static final Logger log=LoggerFactory.getLogger(SessionUtil.class);
	private SessionFactory factory;
	private static SessionUtil instance = null;
	private static final Semaphore semaphore = new Semaphore(1,true);

	private SessionUtil(){
		StandardServiceRegistry registry=null;
		try {
			Configuration cfg=new Configuration().configure("hiber_config/hibernate.cfg.xml");
			registry=cfg.getStandardServiceRegistryBuilder().build();
			factory=new MetadataSources(registry).buildMetadata().buildSessionFactory();
			log.info("SessionFactory was initialized");
		}catch(Exception ex) {
			StandardServiceRegistryBuilder.destroy(registry);
			log.warn("SessionFactory wasnt initialized",ex);
		}
	}

	private static SessionUtil getInstance(){
		try{
			semaphore.acquire();
			if(instance == null){
				instance = new SessionUtil();
			}
			return instance;
		}catch(Exception ex){
			log.error("getInstance",ex);
			return instance;
		}finally{
			semaphore.release();
		}
	}

	private static SessionFactory getSessionFactory() {
		return getInstance().factory;
	}
	
	public static Session getSession() {
		return getSessionFactory().openSession();
	}
	
	public static Session getCurrentSession() {
		return getSessionFactory().getCurrentSession();
	}
	
	public static void shutdown() {
		getSessionFactory().close();
	}
}
