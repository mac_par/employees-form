package com.employees.model.dal;

import javax.persistence.RollbackException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.List;
import com.employees.model.entities.Country;
import com.employees.model.entities.Country_;
import com.employees.model.entities.Department;
import com.employees.model.entities.Department_;
import com.employees.model.entities.Employee;
import com.employees.model.entities.Employee_;
import com.employees.model.entities.Job;
import com.employees.model.entities.Job_;
import com.employees.model.entities.Location;
import com.employees.model.entities.Location_;
import org.hibernate.Hibernate;
import org.hibernate.Transaction;
import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.hibernate.Transaction;

public class EmployeeService implements EmployeeCrud {
	private static final Logger logger = LoggerFactory.getLogger(EmployeeService.class);

	@Override
	public long employeeCount() {
		long result = -1;
		Transaction tx = null;
		try (Session session = SessionUtil.getSession()) {
			tx = session.beginTransaction();
			result = session.createNamedQuery(Employee.countAll, Long.class).uniqueResult();
			tx.commit();
			return result;
		} catch (Exception ex) {
			if (tx != null)
				tx.rollback();
			logger.error("employeeCount()", ex);
		}finally {
			return result;
		}
	}

	private <T extends java.io.Serializable> List<T> getEntityList(Class<T> clazz, String namedQuery){
		Transaction tx = null;
		List<T> list=null;
		try (Session session = SessionUtil.getSession()) {
			tx = session.beginTransaction();
			list= session.createNamedQuery(namedQuery, clazz).list();
			tx.commit();
		} catch (Exception ex) {
			if (tx != null)
				tx.rollback();
			logger.warn(String.format("getEntityList<%s>, NamedQuery: %s",clazz.getName(),namedQuery), ex);
		}finally {
			return list;
		}
	}

	private <T extends java.io.Serializable,R> T getEntityItem(Class<T> clazz, String namedQueryName, R id){
		Transaction tx = null;
		T entity = null;
		try (Session session = SessionUtil.getSession()) {
			tx = session.beginTransaction();
			entity = session.createNamedQuery(namedQueryName, clazz).setParameter("id", id)
					.uniqueResult();
		} catch (Exception ex) {
			if (tx != null)
				tx.rollback();
			logger.error(String.format("getEntityItem<%s>, namedQueryName: %s, id: %d",
					clazz.getName(),namedQueryName,id), ex);
		}finally {
			return entity;
		}
	}

	@Override
	public Employee findEmployee(Long id) {
		return getEntityItem(Employee.class,Employee.findEmployeeById,id);
	}

	@Override
	public List<Employee> getEmployees() {
		return getEntityList(Employee.class,Employee.findAll);
	}

	@Override
	public List<Employee> getEmployeesWithDetails() {
		return getEntityList(Employee.class,Employee.findAllWithDetails);
	}

	@Override
	public List<Employee> findEmployees(String jobId, Long departmentId, Long locationId, String countryId,
			Integer offset, Integer maxResults) {
		Transaction tx = null;
		List<Employee> employees=null;
		try (Session session = SessionUtil.getSession()) {
			tx = session.beginTransaction();
			CriteriaBuilder cb = session.getCriteriaBuilder();
			CriteriaQuery<Employee> q = cb.createQuery(Employee.class);
			Root<Employee> root = q.from(Employee.class);
			q.select(root).distinct(true);
			// joins
			root.fetch(Employee_.job, JoinType.LEFT);
			root.fetch(Employee_.manager,JoinType.LEFT);
			root.fetch(Employee_.department, JoinType.LEFT).fetch(Department_.location, JoinType.LEFT)
					.fetch(Location_.country, JoinType.LEFT);
			Join<Employee, Job> fromJob = root.join(Employee_.job);
			Join<Employee, Department> fromDepart = root.join(Employee_.department, JoinType.LEFT);
			Join<Department, Location> fromLocation = fromDepart.join(Department_.location, JoinType.LEFT);
			Join<Location, Country> fromCountry = fromLocation.join(Location_.country, JoinType.LEFT);

			Predicate p = cb.conjunction();
			if (jobId != null) {
				p = cb.and(p, cb.equal(fromJob.get(Job_.id), jobId.trim()));
			}

			if (departmentId != null) {
				p = cb.and(p, cb.equal(fromDepart.get(Department_.id), departmentId));
			}

			if (locationId != null) {
				p = cb.and(p, cb.equal(fromLocation.get(Location_.id), locationId));
			}

			if (countryId != null) {
				p = cb.and(p, cb.equal(fromCountry.get(Country_.id), countryId.trim()));
			}
			q.where(p);

			if (offset == null && maxResults == null) {
				employees = session.createQuery(q).list();
			} else if (offset == null) {
				employees = session.createQuery(q).setMaxResults(maxResults).list();
			} else if (maxResults == null) {
				employees = session.createQuery(q).setFirstResult(offset).list();
			} else {
				employees = session.createQuery(q).setFirstResult(offset).setMaxResults(maxResults).list();
			}
			tx.commit();

		} catch (Exception ex) {
			if (tx != null)
				tx.rollback();
			logger.warn("getEmployees(...) ", ex);
		}finally {
			return employees;
		}
	}

	private <T extends java.io.Serializable> T saveEntity(T entity){
		Transaction tx = null;
		try (Session session = SessionUtil.getSession()) {
			tx = session.beginTransaction();
			session.save(entity);
			tx.commit();
			return entity;
		} catch (RollbackException ex) {
			if (tx != null)
				tx.rollback();
			logger.warn(String.format("saveEntity<%s>",entity.getClass().getName()), ex);
			throw ex; //TODO check whether throwning exception is needed
		}
	}

	private <T extends java.io.Serializable> void removeEntity(T entity){
		Transaction tx = null;
		try (Session session = SessionUtil.getSession()) {
			tx = session.beginTransaction();
			session.remove(entity);
			tx.commit();
		} catch (RollbackException ex) {
			if (tx != null)
				tx.rollback();
			logger.warn(String.format("removeEntity<%s>",entity.getClass()), ex);
			throw ex;
		}
	}

	@Override
	public Employee addEmployee(Employee employee) {
		return saveEntity(employee);
	}

	@Override
	public void removeEmployee(Employee employee) {
		removeEntity(employee);
	}

	private <T extends java.io.Serializable> T modifyEntity(T entity){
		Transaction tx = null;
		try (Session session = SessionUtil.getSession()) {
			tx = session.beginTransaction();
			session.update(entity);
			tx.commit();
			return entity;
		} catch (RollbackException ex) {
			if (tx != null)
				tx.rollback();
			logger.warn(String.format("modifyEntity<%s>",entity.getClass().getName()), ex);
			throw ex;
		}
	}

	@Override
	public Employee modifyEmployee(Employee employee) {
		return modifyEntity(employee);
	}

	@Override
	public Employee findManager(long employeeId) {
		Employee employee = getEntityItem(Employee.class,Employee.findEmployeeById,employeeId);
		Hibernate.initialize(employee.getManager());
		return employee.getManager();
	}

	@Override
	public List<Employee> findManagers() {
		Transaction tx = null;
		List<Employee> emps=null;
		try (Session session = SessionUtil.getSession()) {
			tx = session.beginTransaction();
			CriteriaBuilder cb = session.getCriteriaBuilder();
			CriteriaQuery<Employee> q = cb.createQuery(Employee.class);
			Root<Employee> root = q.from(Employee.class);
			root.fetch(Employee_.job, JoinType.LEFT);
			root.fetch(Employee_.department, JoinType.LEFT).fetch(Department_.location, JoinType.LEFT)
					.fetch(Location_.country, JoinType.LEFT);
			q.select(root).distinct(true);

			//get Steven King
			CriteriaQuery<Employee> qHead = cb.createQuery(Employee.class);
			Root<Employee> headManagerRoot = qHead.from(Employee.class);
			qHead.select(headManagerRoot).distinct(true);
			qHead.where(cb.isNull(headManagerRoot.get(Employee_.manager)));
			Employee manager = session.createQuery(qHead).setMaxResults(1).getSingleResult();

			logger.debug("Manager to: "+manager.toString());
			Predicate p = cb.equal(root.get(Employee_.manager), manager);
			q.where(p);
			emps = session.createQuery(q).list();
			tx.commit();
		} catch (Exception ex) {
			System.out.println(ex);
			System.exit(1);
			if (tx != null)
				tx.rollback();
			logger.error("findManagers()", ex);
		}finally {
			return emps;
		}
	}

	@Override
	public List<Department> getDepartments() {
		return getEntityList(Department.class,Department.findAll);
	}

	@Override
	public List<Job> getJobs() {
		return getEntityList(Job.class,Job.findAll);
	}

	@Override
	public List<Location> getLocations() {
		return getEntityList(Location.class,Location.findAll);
	}

	@Override
	public List<Country> getCountries() {
		return getEntityList(Country.class,Country.findAll);
	}

	@Override
	public void close() {
		SessionUtil.shutdown();
		logger.info("SessionUtil was turned off");
	}

	@Override
	public Department getDepartments(Long id) {
		return getEntityItem(Department.class,Department.findById,id);
	}

	@Override
	public Job getJob(String id) {
		return getEntityItem(Job.class,Job.findById,id);
	}

	@Override
	public Location getLocation(Long id) {
		return getEntityItem(Location.class,Location.findById,id);
	}

	@Override
	public Country getCountry(String id) {
		return getEntityItem(Country.class,Country.findById,id);
	}

}
