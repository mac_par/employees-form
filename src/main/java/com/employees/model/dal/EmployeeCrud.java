package com.employees.model.dal;
import com.employees.model.entities.*;
import java.util.List;


public interface EmployeeCrud {
	long employeeCount();
	Employee findEmployee(Long id);
	List<Employee> getEmployees();
	List<Employee> getEmployeesWithDetails();
	List<Employee> findEmployees(String jobId,Long departmentId, Long locationId,String countryId,Integer offset,Integer maxResults);
	Employee addEmployee(Employee employee);
	void removeEmployee(Employee employee);
	Employee modifyEmployee(Employee employee);
	Employee findManager(long employeeId);
	List<Employee> findManagers();
	Department getDepartments(Long id);
	List<Department> getDepartments();
	Job getJob(String id);
	List<Job> getJobs();
	Location getLocation(Long id);
	List<Location> getLocations();
	Country getCountry(String id);
	List<Country> getCountries();
	void close();
}
